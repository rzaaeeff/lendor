package it.unipv.lending.view;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import it.unipv.lending.model.User;
import it.unipv.lending.service.UserRepository;
import it.unipv.lending.utility.Utility;

@Named
@RequestScoped
public class LoginView {

	@Inject
	private UserRepository userRepository;

	private String username;
	private String password;

	// do the login part
	public String login() {
		if (checkIfEmpty()) {
			return "login.xhtml";
		}

		String passwordHash = Utility.hashSha256(password);
		FacesContext context = FacesContext.getCurrentInstance();

		User userLogged = userRepository.getUserbyUsername(username, passwordHash);

		if (userLogged == null) {
			context.addMessage(null, new FacesMessage("Unknown login, try again"));
			username = null;
			password = null;
			return "login.xhtml?faces-redirect=true";
		}

		else {
			System.out.println("LOGIN");
			System.out.println(userLogged.getUsername());

			context.getExternalContext().getSessionMap().clear();
			context.getExternalContext().getSessionMap().put("userLogged", userLogged);
			return "userMenu.xhtml?faces-redirect=true";
		}

	}

	private boolean checkIfEmpty() {
		FacesMessage message = new FacesMessage("Field cannot be empty.");
		boolean isAnyEmpty = false;

		if (username.isEmpty()) {
			FacesContext.getCurrentInstance().addMessage("loginForm:j_username", message);

			isAnyEmpty = true;
		}

		if (password.isEmpty()) {
			FacesContext.getCurrentInstance().addMessage("loginForm:j_password", message);

			isAnyEmpty = true;
		}

		return isAnyEmpty;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String goToRegistration() {
		return "registration.xhtml?faces-redirect=true";
	}
	
	
	public String logout() {
		FacesContext context = FacesContext.getCurrentInstance();
		context.getExternalContext().getSessionMap().put("userLogged", null);
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
		return "login.xhtml?faces-redirect=true";
	}

}
