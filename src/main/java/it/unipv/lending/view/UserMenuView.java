package it.unipv.lending.view;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

@Named("UserMenuView")
@RequestScoped

public class UserMenuView {

	public String goToObjectList() {

		return "productsList.xhtml?faces-redirect=true";
	}

	public String goToContractList() {

		return "leaseContract.xhtml?faces-redirect-true";
	}

}
