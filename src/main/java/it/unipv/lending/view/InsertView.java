package it.unipv.lending.view;

import java.io.Serializable;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import it.unipv.lending.model.Product;
import it.unipv.lending.service.ProductRepository;

@Named
@RequestScoped
public class InsertView implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private String name;
	private String description;

	
	@Inject
	private ProductRepository service;

	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String addProduct() {

		
		Product product = new Product(this.name,this.description);
		service.addProduct(product);

		return "productsList.xhtml?faces-redirect=true";
	}

	public String goToProductList() {
		return "productsList.xhtml?faces-redirect=true";
	}

}