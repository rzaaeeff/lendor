package it.unipv.lending.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;


@Entity
public class User implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long userId;

	private String name;
	private String surname;
	private String password;
	private String username;
	private String email;
	private String city;

	//Relationship attributes
	
	@OneToMany(fetch= FetchType.EAGER, mappedBy = "user",cascade = CascadeType.ALL,orphanRemoval = true)
	private List<Product> products;
	
	@OneToMany(mappedBy= "renter",cascade=CascadeType.ALL,orphanRemoval=true)
	private List<LeaseContract> contractsAsRenter;

	@OneToMany(mappedBy= "lender",cascade=CascadeType.ALL,orphanRemoval=true)
	private List<LeaseContract> contractsAsLender;
	
	public User() {
		products = new ArrayList<>();
		this.contractsAsLender=new ArrayList<>();
		this.contractsAsRenter=new ArrayList<>();
	}
	
	public List<LeaseContract> getContractsAsLender(){
		
		return this.contractsAsLender;
	}
	
    public List<LeaseContract> getContractsAsRenter(){
		
		return this.contractsAsRenter;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	
	public String getPassword() {
		return password;
	}

	public String getName() {
		return name;
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<Product> getProducts() {
		return products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}

	public void setName(String name) {
		// TODO Auto-generated method stub
		this.name = name;
	}

	public void setPassword(String password) {
		// TODO Auto-generated method stub
		this.password = password;
		
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

}
