package it.unipv.lending.test;

import java.io.File;

import org.apache.log4j.Logger;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.OverProtocol;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ArchivePaths;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.exporter.ZipExporter;
import org.jboss.shrinkwrap.api.spec.WebArchive;

import it.unipv.lending.test.ArquillianTest;

public class ArquillianTest {

	
	Logger log = Logger.getLogger(ArquillianTest.class);
	
	@Deployment(name = "LendingTest")
	@OverProtocol("Servlet 3.0") 
	
	public static Archive<?> createDeployment() {
	
		WebArchive archive = ShrinkWrap.create(WebArchive.class, "LendingTest.war")
        .addPackages(true, "it.unipv.lending.service")
        .addPackages(true, "it.unipv.lending.model")
        .addPackages(true, "it.unipv.lending.test")
        .addPackages(true,"it.unipv.lending.utility")
        
        .addAsResource("META-INF/persistence.xml")
        .addAsWebInfResource(EmptyAsset.INSTANCE, ArchivePaths.create("beans.xml"));
		
		// Esportazione di prova per controllo
		  archive.as(ZipExporter.class).exportTo(
				    new File("target/arquillianPackage.war"), true);
		
		return archive;
		
	}
}
